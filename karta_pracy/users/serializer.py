from rest_framework import serializers
from .models import ExtendedUser, Specialization
from djoser.serializers import UserCreateSerializer as BaseUserCreateSerializer, UserSerializer as BaseUserSerializer

from django.contrib.auth.models import User


class UserCreateSerializer(BaseUserCreateSerializer):
    def create(self, validated_data):
        user = super(UserCreateSerializer, self).create(validated_data)
        user.is_active = False
        user.save(update_fields=["is_active"])
        return user

    class Meta(BaseUserCreateSerializer.Meta):
        fields = ['id', 'username', 'password',
                  'email', 'first_name', 'last_name']


class UserSerializerPartial(BaseUserSerializer):
    class Meta(BaseUserSerializer.Meta):
        fields = ['first_name', 'last_name']


class ExtendedUserPartialSerializer(serializers.ModelSerializer):
    specialization = serializers.PrimaryKeyRelatedField(
        queryset=Specialization.objects.all(), many=False)
    name = UserSerializerPartial(
        many=False, read_only=True, source="user")

    class Meta:
        model = ExtendedUser
        fields = ['id', 'name', 'specialization']


class UserSerializer(BaseUserSerializer):
    extended_user = ExtendedUserPartialSerializer()

    class Meta(BaseUserSerializer.Meta):
        fields = ['id', 'username', 'email', 'first_name',
                  'last_name', 'is_staff', 'is_superuser', "extended_user"]


class ExtendedUserSerializer(serializers.ModelSerializer):
    specialization = serializers.PrimaryKeyRelatedField(
        queryset=Specialization.objects.all(), many=False)
    user = UserCreateSerializer(many=False, write_only=True)
    personal_data = UserSerializer(
        many=False, read_only=True, source="user")

    def create(self, validated_data):
        user_data = validated_data.pop('user')
        user_serializer = UserCreateSerializer(data=user_data)
        user_serializer.is_valid(raise_exception=True)
        user_serializer.save()

        user = User.objects.get(pk=user_serializer.data['id'])
        profile = ExtendedUser.objects.create(
            user=user, **validated_data)
        return profile

    class Meta:
        model = ExtendedUser
        fields = ['id', 'user', 'personal_data', 'specialization']


class SpecializationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Specialization
        fields = ["id", "name"]

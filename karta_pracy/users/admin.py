from django.contrib import admin
from .models import Specialization, ExtendedUser


class SpecializationAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']


class ExtendedUserAdmin(admin.ModelAdmin):
    list_display = ['id', 'specialization', 'user']


admin.site.register(Specialization, SpecializationAdmin)
admin.site.register(ExtendedUser, ExtendedUserAdmin)
# Register your models here.

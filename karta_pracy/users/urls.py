from django.urls import path
from rest_framework.routers import DefaultRouter

from .views import *

# @TODO think about more consist way to add urls
router = DefaultRouter()
router.register(r'specialization', SpecializationView,
                basename="specialization")

router.register('extended_users', ExtendedUserViewSet)
router.register('extended_users_partial', ExtendedUserPartialViewSet)
urlpatterns = router.urls

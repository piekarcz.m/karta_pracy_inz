from django.db import models
from django.contrib.auth.models import User


class Specialization(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


# @TODO Add permissions (grups)


class ExtendedUser(models.Model):
    specialization = models.ForeignKey(
        Specialization, null=True, on_delete=models.SET_NULL)
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=False, related_name="extended_user")

    def __str__(self):
        return f'{self.user.first_name} {self.user.last_name}'

    class Meta:
        ordering = ['user__first_name', 'user__last_name']

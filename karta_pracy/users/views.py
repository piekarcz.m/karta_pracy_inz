import json

from django.http import Http404
from django.shortcuts import get_object_or_404
# from django.core.exceptions import ObjectDoesNotExist
from rest_framework import mixins
from rest_framework.generics import GenericAPIView
from rest_framework.viewsets import ModelViewSet, ReadOnlyModelViewSet
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, AllowAny, IsAdminUser, SAFE_METHODS
from rest_framework.decorators import permission_classes, action
from rest_framework import status

from django.contrib.auth.models import User
from .models import ExtendedUser, Specialization
from .serializer import *

# @TODO resolve problem, when user was created but extended user not, now all field in extendeUser are optional
# @TODO We need list model mixin ? -
# @TODO only admin shoul have acces to other users - fix it!


class ExtendedUserViewSet(ModelViewSet):
    serializer_class = ExtendedUserSerializer
    queryset = ExtendedUser.objects.all()

    def get_permissions(self):
        if self.request.method == 'POST':
            return [AllowAny()]
        if self.action == "me":
            return [IsAuthenticated()]
        return [IsAdminUser()]

    def get_queryset(self):
        user = self.request.user
        queryset = super().get_queryset()
        if self.action == "list" and not user.is_staff:
            queryset = queryset.filter(pk=user.pk)
        return queryset

    @action(detail=False, methods=['GET', 'PUT', 'DELETE'])
    def me(self, request):
        extended_user = get_object_or_404(
            ExtendedUser, user_id=request.user.id)
        # @TODO request.method instead of self.request.method ?
        if self.request.method == 'DELETE':
            extended_user.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)

        if self.request.method == 'GET':
            serializer = ExtendedUserSerializer(extended_user)

        if self.request.method == 'PUT':
            serializer = ExtendedUserSerializer(
                extended_user, data=request.data)
            serializer.is_valid(raise_exception=True)
            serializer.save()

        return Response(serializer.data)


class ExtendedUserPartialViewSet(ReadOnlyModelViewSet):
    serializer_class = ExtendedUserPartialSerializer
    queryset = ExtendedUser.objects.all()

    def get_queryset(self):
        queryset = ExtendedUser.objects.all()

        filters = self.request.query_params.get('filters')
        if filters is None:
            return queryset

        filter_json = json.loads(filters)
        if 'is_staff' in filter_json.keys():
            is_staff = filter_json['is_staff']
            queryset = queryset.filter(user__is_staff=is_staff)

        if 'is_admin' in filter_json.keys():
            is_admin = filter_json['is_admin']
            queryset = queryset.filter(user__is_superuser=is_admin)

        if 'is_active' in filter_json.keys():
            is_active = filter_json['is_active']
            queryset = queryset.filter(user__is_active=is_active)

        if 'specialization' in filter_json.keys():
            specialization = json.loads(filter_json['specialization'])
            queryset = queryset.filter(specialization=specialization)

        return queryset


class SpecializationView(ModelViewSet):
    serializer_class = SpecializationSerializer
    queryset = Specialization.objects.all()

    def get_permissions(self):
        if self.request.method in SAFE_METHODS:
            return [AllowAny()]
        # @TODO change this for concrete permission (in example isStaff)
        return [IsAdminUser()]

from django.urls import path
from rest_framework.routers import DefaultRouter

from .views import *

router = DefaultRouter()
router.register(r'airship_type', AirShipTypeViewSet,
                basename="airship_type")
router.register(r'airship', AirShipViewSet, basename="airship")
router.register(r'handlings_level', HandlingsLevelViewSet,
                basename="handlings_level")
router.register(r'card_header', CardHeaderViewSet,
                basename="card_header")
router.register(r'technologic_card', TechnologicCardViewSet,
                basename="technologic_card")
router.register(r'jzot', JzotViewSet,
                basename="jzot")
router.register(r'card_handlings', CardHandlingsViewSet,
                basename="card_handlings")

urlpatterns = [path("complete_execute/<int:id>",
                    complete_execute, name="complete_execute")]

urlpatterns += router.urls

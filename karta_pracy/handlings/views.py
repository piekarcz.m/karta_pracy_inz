import json

from django.shortcuts import get_object_or_404
from django.http import HttpResponse
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import permission_classes, action, api_view
from rest_framework.exceptions import APIException

from .models import *
from .serializer import *
from .permissions import IsAdminOrReadOnly, IsStaffOrReadOnly, CardHandlingsPermission


class AirShipTypeViewSet(ModelViewSet):
    serializer_class = AirShipTypeSerializer
    queryset = AirShipType.objects.all()
    permission_classes = [IsAdminOrReadOnly, IsAuthenticated]


class AirShipViewSet(ModelViewSet):
    serializer_class = AirShipSerializer
    queryset = AirShip.objects.all()
    permission_classes = [IsAdminOrReadOnly, IsAuthenticated]


class HandlingsLevelViewSet(ModelViewSet):
    serializer_class = HandlingsLevelSerializer
    queryset = HandlingsLevel.objects.all()
    permission_classes = [IsAdminOrReadOnly, IsAuthenticated]


class CardHeaderViewSet(ModelViewSet):
    serializer_class = CardHeaderSerializer
    permission_classes = [IsAdminOrReadOnly, IsAuthenticated]

    @action(detail=True, methods=['PATCH'])
    def approve(self, request, pk=None):
        header = self.get_object()

        serializer = CardHeaderSerializer(
            header, data={"approved": request.data['approved']}, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(status=status.HTTP_200_OK)

    @action(detail=True, methods=['PATCH'])
    def finish(self, request, pk=None):
        if 'finished' not in request.data.keys():
            return Response(status=status.HTTP_400_BAD_REQUEST)

        header = self.get_object()
        finished = request.data['finished']

        if finished == True and header.is_completed() and not header.finish_date:
            header.finish_date = date.today()
        elif finished == False:
            header.finish_date = None
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        header.save()
        return Response(status=status.HTTP_200_OK)

    def get_queryset(self):
        queryset = CardHeader.objects.all()

        filters = self.request.query_params.get('filters')
        if filters is None:
            return queryset

        filter_json = json.loads(filters)

        if 'airship' in filter_json.keys():
            airship = json.loads(filter_json['airship'])
            if airship:
                queryset = queryset.filter(air_ship__in=airship)

        if 'airship_type' in filter_json.keys():
            air_ship_types = json.loads(filter_json['airship_type'])
            if air_ship_types:
                queryset = queryset.filter(
                    air_ship__type_id__in=air_ship_types)

        if 'handling_level' in filter_json.keys():
            handling_level = json.loads(filter_json['handling_level'])
            if handling_level:
                queryset = queryset.filter(handlings_level__in=handling_level)

        if 'start_date_from' in filter_json.keys():
            start_date_from = filter_json['start_date_from']
            if start_date_from:
                queryset = queryset.filter(start_date__gte=start_date_from)

        if 'start_date_to' in filter_json.keys():
            start_date_to = filter_json['start_date_to']
            if start_date_to:
                queryset = queryset.filter(start_date__lte=start_date_to)

        return queryset


class TechnologicCardViewSet(ModelViewSet):
    serializer_class = TechnologicCardSerializer
    queryset = TechnologicCard.objects.all()
    permission_classes = [IsAdminOrReadOnly, IsAuthenticated]


class JzotViewSet(ModelViewSet):
    serializer_class = JzotSerializer
    permission_classes = [IsAdminOrReadOnly, IsAuthenticated]

    def get_queryset(self):
        queryset = Jzot.objects.all()

        filters = self.request.query_params.get('filters')
        if filters is None:
            return queryset

        filter_json = json.loads(filters)

        if 'airship_type' in filter_json.keys():
            air_ship_types = json.loads(filter_json['airship_type'])
            if air_ship_types:
                queryset = queryset.filter(airship_type__in=air_ship_types)

        if 'specialization' in filter_json.keys():
            specializations = json.loads(filter_json['specialization'])
            if specializations:
                queryset = queryset.filter(specialization__in=specializations)

        if 'handlings_level' in filter_json.keys():
            handlings_levels = json.loads(filter_json['handlings_level'])
            if handlings_levels:
                queryset = queryset.filter(
                    handlings_level__in=handlings_levels)

        return queryset


class CardHandlingsViewSet(ModelViewSet):
    # @TODO mayby add posibitilty to change or dalete permission only for user, who create that card, and staffs
    serializer_class = CardHandlingsSerializer
    permission_classes = [IsStaffOrReadOnly, CardHandlingsPermission]

    def create(self, request, *args, **kwargs):
        try:
            return super().create(request, *args, **kwargs)
        except IntegrityError as exc:
            raise APIException(detail=exc)

    def update(self, request, *args, **kwargs):
        try:
            return super().create(request, *args, **kwargs)
        except IntegrityError as exc:
            raise APIException(detail=exc)

    def get_queryset(self):
        queryset = CardHandlings.objects.all()

        filters = self.request.query_params.get('filters')
        if filters is None:
            return queryset

        filter_json = json.loads(filters)
        if 'header_id' in filter_json.keys():
            header_id = json.loads(filter_json['header_id'])
            if header_id:
                queryset = queryset.filter(header=header_id)

        if 'supervisor' in filter_json.keys():
            supervisor = json.loads(filter_json['supervisor'])
            if supervisor:
                queryset = queryset.filter(supervisier__in=supervisor)

        if 'executor' in filter_json.keys():
            executor = json.loads(filter_json['executor'])
            if executor:
                queryset = queryset.filter(execute__executor__in=executor)

        if 'specialization' in filter_json.keys():
            specializations = json.loads(filter_json['specialization'])
            if specializations:
                queryset = queryset.filter(
                    jzot__specialization__in=specializations)

        if 'with_completed' not in filter_json.keys() or filter_json['with_completed'] != True:
            queryset = queryset.filter(finish_date__isnull=True)

        # For Executors shows only his Card Handling, which is not completed
        # and which header is approved
        if not self.request.user.is_staff and not self.request.user.is_superuser:
            queryset = queryset.filter(
                execute__executor__in=[self.request.user.extended_user.id],
                execute__state=False,
                header__approved=True)

        return queryset


@permission_classes([IsAuthenticated])
@api_view(['PATCH'])
def complete_execute(request, id):
    execute = get_object_or_404(
        Execute, id=id)

    # Only Executor can change his execution
    if execute.executor_id != request.user.extended_user.id:
        return Response(status=status.HTTP_403_FORBIDDEN)

    # Execute only handlings, which header is approved
    if not execute.card_handling.header.approved:
        return Response(status=status.HTTP_403_FORBIDDEN)

    try:
        serializer = ExecuteSerializer(
            execute, data={"state": request.data['state']}, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
    except IntegrityError as exc:
        return Response(status=status.HTTP_400_BAD_REQUEST, data={"error": str(exec)})

    return Response(status=status.HTTP_200_OK)
# Create your views here.

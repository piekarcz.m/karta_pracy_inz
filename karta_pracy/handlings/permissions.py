from rest_framework.permissions import BasePermission, SAFE_METHODS
from .models import CardHeader
from django.shortcuts import get_object_or_404


class IsAdminOrReadOnly(BasePermission):
    """
    The request is authenticated as a admin, or is a read-only request.
    """

    def has_permission(self, request, view):
        return bool(
            request.method in SAFE_METHODS or
            request.user and
            request.user.is_superuser
        )


class IsStaffOrReadOnly(BasePermission):
    """
    The request is authenticated as a staff, or is a read-only request.
    """

    def has_permission(self, request, view):
        return bool(
            request.method in SAFE_METHODS or
            request.user and
            request.user.is_staff
        )


# HACK use only for Card handlings
class EditOnlyForOwnerOrAdmin(BasePermission):
    def has_object_permission(self, request, view, obj):

        if request.method in ('PATCH', 'PUT'):
            return bool(
                request.user and
                (request.user.is_superuser or request.user.extended_user.id == obj.supervisier.id))

        return True


class CardHandlingsPermission(BasePermission):
    def can_edit(self, request, obj):
        prohibited_fields = ["jzot", "effort_hours"]
        if obj.header.finish_date:
            return False

        if not obj.header.approved:
            return True

        if "jzot" in request.data.keys() and obj.jzot.id != request.data['jzot']:
            return False
        if "effort_hours" in request.data.keys() and obj.effort_hours != request.data['effort_hours']:
            return False

        return True

    def has_object_permission(self, request, view, obj):
        if request.method in SAFE_METHODS:
            return True

        if not request.user:
            return False

        user_permmit = (
            request.user.is_superuser or request.user.extended_user.id == obj.supervisier.id)

        if request.method in ('PATCH', 'PUT'):
            return bool(user_permmit and self.can_edit(request, obj))

        if request.method in ('DELETE'):
            return user_permmit and (not (obj.header.finish_date or obj.header.approved))

        return True

    def has_permission(self, request, view):
        if request.method == 'POST':
            if 'header' not in request.data.keys():
                return False

            header = CardHeader.objects.get(id=request.data['header'])
            if not header:
                return False

            return not (header.finish_date or header.approved)

        return True

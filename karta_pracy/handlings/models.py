import os
from datetime import date
from django.contrib.auth.models import User
from django.db import models,  IntegrityError
from django.db.models.signals import post_save, pre_save
from users.models import ExtendedUser, Specialization

# @TODO Add constains (to achieve consistance)


class AirShipType(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class AirShip(models.Model):
    number = models.CharField(max_length=200, unique=True)
    type_id = models.ForeignKey(
        AirShipType, null=False, on_delete=models.RESTRICT, db_column='type_id')

    def __str__(self):
        return self.number + " " + str(self.type_id)


# @TODO z tą tabelą coś może być nie tak, po co tutaj air_ship ? no albo contstain trzeba dodac


class HandlingsLevel(models.Model):
    level = models.CharField(max_length=200)
    air_ship_type = models.ForeignKey(
        AirShipType, null=False, on_delete=models.RESTRICT)

    def __str__(self):
        return self.level + " " + str(self.air_ship_type)


class CardHeader(models.Model):
    number = models.CharField(max_length=200, unique=True)
    # @TODO mayby We should also specify related_query_name ?
    issuer = models.ForeignKey(
        User, null=False, on_delete=models.RESTRICT, related_name="%(app_label)s_%(class)s_issuers")
    handlings_level = models.ForeignKey(
        HandlingsLevel, null=False, on_delete=models.RESTRICT)
    air_ship = models.ForeignKey(
        AirShip, null=False, on_delete=models.RESTRICT)
    start_date = models.DateField(null=True)
    finish_date = models.DateField(null=True)
    man_hours = models.FloatField(null=True)
    approved = models.BooleanField(default=False)

    def is_completed(self):
        return self.cardhandlings_set.filter(finish_date__isnull=True).count() == 0

    def man_hours_calculate(self):
        handlings = list(self.cardhandlings_set.all())
        man_hours = 0
        for record in handlings:
            man_hours += record.man_hours if record.man_hours else 0

        self.man_hours = man_hours
        self.save()

    @staticmethod
    def finish_date_update(instance):
        if not instance.finish_date and instance.is_completed():
            instance.finish_date = date.today()
            instance.save()

        if instance.finish_date and not instance.is_completed():
            instance.finish_date = None
            instance.save()

# @TODO add specialization id
# @TODO add constrain to jzot table


class TechnologicCard(models.Model):
    number = models.CharField(max_length=200, unique=True)
    pdf_card = models.FileField(upload_to='technologic_cards/')

    def __str__(self):
        return self.pdf_card.url

    def delete(self, *args, **kwargs):
        self.pdf_card.delete()
        super().delete(*args, **kwargs)


class Jzot(models.Model):
    airship_type = models.ForeignKey(
        AirShipType, null=False, on_delete=models.RESTRICT)
    specialization = models.ForeignKey(
        Specialization, null=False, on_delete=models.RESTRICT)
    system = models.CharField(max_length=200)
    aggregate = models.CharField(max_length=200)
    # @TODO add validate regexp
    point_number = models.CharField(max_length=200)
    point_text = models.TextField()
    technologic_card = models.ForeignKey(
        TechnologicCard, null=False, on_delete=models.RESTRICT)
    handlings_level = models.ForeignKey(
        HandlingsLevel, null=False, on_delete=models.RESTRICT)


class CardHandlings(models.Model):
    header = models.ForeignKey(
        CardHeader, null=False, on_delete=models.CASCADE)
    jzot = models.ForeignKey(Jzot, null=False, on_delete=models.RESTRICT)
    effort_hours = models.FloatField(null=True)
    man_hours = models.FloatField(null=True)
    finish_date = models.DateField(null=True)
    supervisier = models.ForeignKey(
        ExtendedUser, null=True, on_delete=models.SET_NULL, related_name="%(app_label)s_%(class)s_cards_to_supervise")

    def is_completed(self):
        queryset = self.execute_set.filter(state=False)
        return len(queryset) == 0

    @staticmethod
    def finish_date_update(instance):
        if not instance.finish_date and instance.is_completed():
            instance.finish_date = date.today()
            instance.save()

        if instance.finish_date and not instance.is_completed():
            instance.finish_date = None
            instance.save()

    @staticmethod
    def after_save(sender, instance, **kwargs):
        CardHandlings.finish_date_update(instance)
        instance.header.man_hours_calculate()

    @staticmethod
    def before_save(sender, instance, **kwargs):
        if instance.jzot.specialization != instance.supervisier.specialization:
            raise IntegrityError("Superviser specialization don't match")

        if instance.jzot.handlings_level != instance.header.handlings_level:
            raise IntegrityError("Handlings Levels in jzot and header\
                                  are not consistent")

        if instance.jzot.airship_type != instance.header.air_ship.type_id:
            raise IntegrityError("Jzot airship type is not match\
                                  to headers airship")


class Execute(models.Model):
    executor = models.ForeignKey(
        ExtendedUser, null=False, on_delete=models.RESTRICT)
    card_handling = models.ForeignKey(
        CardHandlings, null=False, on_delete=models.CASCADE)
    state = models.BooleanField()

    @staticmethod
    def after_save(sender, instance, **kwargs):
        CardHandlings.finish_date_update(instance.card_handling)

    @staticmethod
    def before_save(sender, instance, **kwargs):
        if instance.executor.specialization != instance.card_handling.jzot.specialization:
            raise IntegrityError("Executor specialization don't match")


# @TODO consider how to use only one signal, it is probably redundance
post_save.connect(Execute.after_save, sender=Execute)
post_save.connect(CardHandlings.after_save, sender=CardHandlings)

pre_save.connect(Execute.before_save, sender=Execute)
pre_save.connect(CardHandlings.before_save, sender=CardHandlings)

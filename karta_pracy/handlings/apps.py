from django.apps import AppConfig


class HandlingsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'handlings'

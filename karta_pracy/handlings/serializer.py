from rest_framework import serializers
from .models import AirShipType, AirShip, HandlingsLevel, CardHeader, TechnologicCard, Jzot, CardHandlings, Execute
from django.contrib.auth.models import User
from users.models import ExtendedUser, Specialization
from users.serializer import ExtendedUserPartialSerializer, SpecializationSerializer, UserSerializerPartial

# @TODO Mayby add some nested serializer


class AirShipTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = AirShipType
        fields = ["id", "name"]


class AirShipSerializer(serializers.ModelSerializer):
    type_id = serializers.PrimaryKeyRelatedField(
        queryset=AirShipType.objects.all(), many=False)
    type_serialized = AirShipTypeSerializer(
        many=False, read_only=True, source="type_id")

    class Meta:
        model = AirShip
        fields = ["id", "number", "type_id", "type_serialized"]


class HandlingsLevelSerializer(serializers.ModelSerializer):
    air_ship_type = serializers.PrimaryKeyRelatedField(
        queryset=AirShipType.objects.all(), many=False)

    air_ship_type_serialized = AirShipTypeSerializer(
        many=False, read_only=True, source="air_ship_type")

    class Meta:
        model = HandlingsLevel
        fields = ["id", "level", "air_ship_type", "air_ship_type_serialized"]


class CardHeaderSerializer(serializers.ModelSerializer):
    air_ship = serializers.PrimaryKeyRelatedField(
        queryset=AirShip.objects.all(), many=False)
    airship_serialized = AirShipSerializer(
        many=False, read_only=True, source="air_ship")

    handlings_level = serializers.PrimaryKeyRelatedField(
        queryset=HandlingsLevel.objects.all(), many=False)
    handlings_level_serialized = HandlingsLevelSerializer(
        many=False, read_only=True, source="handlings_level")

    man_hours = serializers.FloatField(read_only=True)

    issuer = serializers.PrimaryKeyRelatedField(
        queryset=User.objects.all(), many=False)

    issuer_partial_serialized = UserSerializerPartial(
        many=False, read_only=True, source="issuer")

    complete_level = serializers.SerializerMethodField()

    def get_complete_level(self, instance):
        all_objs = instance.cardhandlings_set.all().count()
        completed = instance.cardhandlings_set.filter(
            finish_date__isnull=False).count()

        return "{}/{}".format(completed, all_objs)

    class Meta:
        model = CardHeader
        fields = ["id", "number", "issuer",
                  "handlings_level", "handlings_level_serialized", "air_ship", "airship_serialized",
                  "start_date", "finish_date", "man_hours",
                  "issuer_partial_serialized", "complete_level", "approved"]


class TechnologicCardSerializer(serializers.ModelSerializer):
    class Meta:
        model = TechnologicCard
        fields = ["id", "number", "pdf_card"]


class JzotSerializer(serializers.ModelSerializer):
    airship_type = serializers.PrimaryKeyRelatedField(
        queryset=AirShipType.objects.all(), many=False)
    airship_type_serialized = AirShipTypeSerializer(
        many=False, read_only=True, source="airship_type")

    specialization = serializers.PrimaryKeyRelatedField(
        queryset=Specialization.objects.all(), many=False)
    specialization_serialized = SpecializationSerializer(
        many=False, read_only=True, source="specialization")

    technologic_card_url = serializers.FileField(
        read_only=True, source="technologic_card.pdf_card")
    technologic_card = serializers.PrimaryKeyRelatedField(
        queryset=TechnologicCard.objects.all(), many=False)

    handlings_level = serializers.PrimaryKeyRelatedField(
        queryset=HandlingsLevel.objects.all(), many=False)
    handlings_level_serialized = HandlingsLevelSerializer(
        many=False, read_only=True, source="handlings_level")

    class Meta:
        model = Jzot
        fields = ["id", "airship_type", "airship_type_serialized", "system", "specialization", "specialization_serialized",
                  "system", "aggregate", "point_number", "point_text", "technologic_card", "technologic_card_url",
                  "handlings_level", "handlings_level_serialized"]


class ExecuteSerializer(serializers.ModelSerializer):
    executor = serializers.PrimaryKeyRelatedField(
        queryset=ExtendedUser.objects.all(), many=False, write_only=True)

    executor_partial_serialized = ExtendedUserPartialSerializer(
        many=False, read_only=True, source="executor")

    class Meta:
        model = Execute
        fields = ["id", "executor", "executor_partial_serialized",
                  "card_handling", "state"]


class CardHandlingsSerializer(serializers.ModelSerializer):
    jzot = serializers.PrimaryKeyRelatedField(
        queryset=Jzot.objects.all(), many=False)

    header = serializers.PrimaryKeyRelatedField(
        queryset=CardHeader.objects.all(), many=False)

    supervisier_partial_serialized = ExtendedUserPartialSerializer(
        many=False, read_only=True, source="supervisier")

    executors = serializers.PrimaryKeyRelatedField(
        queryset=ExtendedUser.objects.all(), many=True, write_only=True)

    supervisier = serializers.PrimaryKeyRelatedField(
        queryset=ExtendedUser.objects.all(), many=False)

    execute_set = ExecuteSerializer(many=True, read_only=True)
    header_approved = serializers.SerializerMethodField()
    header_number = serializers.SerializerMethodField()
    specialization = serializers.SerializerMethodField()

    def get_executors(self, instance):
        return Execute.objects.filter(
            card_handling_id=instance.id).values_list('executor_id', flat=True)

    def get_header_approved(self, instance):
        return instance.header.approved

    def get_header_number(self, instance):
        return instance.header.number

    def get_specialization(self, instance):
        return instance.jzot.specialization.name

    def create(self, validated_data):
        executors = validated_data.pop('executors')
        card_handling = CardHandlings.objects.create(**validated_data)

        for e in executors:
            Execute.objects.create(
                executor=e, card_handling=card_handling, state=False)

        return card_handling

    # @TOOD after any update(edit) every execute state is set to False, consider to change this
    def update(self, instance, validated_data):
        executors = validated_data.pop('executors')
        Execute.objects.filter(card_handling_id=instance.id).delete()

        for e in executors:
            Execute.objects.create(
                executor=e, card_handling=instance, state=False)

        instance = super(CardHandlingsSerializer, self).update(
            instance, validated_data)
        return instance

    class Meta:
        model = CardHandlings
        fields = ["id", "header", "jzot", "effort_hours", "man_hours",
                  "finish_date", "supervisier",
                  "executors",
                  "supervisier_partial_serialized", "execute_set",
                  "header_approved", "header_number", "specialization"]

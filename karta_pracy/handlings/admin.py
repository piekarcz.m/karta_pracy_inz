from django.contrib import admin
from .models import AirShipType, AirShip, HandlingsLevel


class AirShipTypeAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']


class AirShipAdmin(admin.ModelAdmin):
    list_display = ['id', 'number', 'type_id']


class HandlingsLevelAdmin(admin.ModelAdmin):
    list_display = ['id', 'level', 'air_ship_type']


admin.site.register(AirShipType, AirShipTypeAdmin)
admin.site.register(AirShip, AirShipAdmin)
admin.site.register(HandlingsLevel, HandlingsLevelAdmin)

# Register your models here.

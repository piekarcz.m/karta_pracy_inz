import axios from "axios";
import { createStore } from 'vuex'
import { IsTokenValid } from "../utils/authUtils";
import { setAuthorizationToken } from "../utils/authUtils";

const user_url = "http://127.0.0.1:8000/auth/users/me/";

// Create a new store instance.
const store = createStore({
    state () {
      return {
        isAuthenticated: IsTokenValid(localStorage.getItem("jwtAccessToken")),
        user: JSON.parse(localStorage.getItem("user"))
      }
    },
    mutations: {
      updateLogin(state) {
        state.isAuthenticated = IsTokenValid(localStorage.getItem("jwtAccessToken"));
      },
      updateUser(state) {
        setAuthorizationToken(localStorage.getItem("jwtAccessToken"));
        axios.get(user_url)
        .then(res => {
          console.log("user response: ", res);
          state.user = res.data;
          localStorage.setItem("user", JSON.stringify(res.data));
        })
        .catch(error => {
          console.log("Error while trying get user data: ", error);
          state.user = null;
        });
      },
      clearUser(state) {
        state.user = null;
        localStorage.removeItem("user");
      }
    }
  });

  export function getUserType() {
    if (store.state.user?.is_superuser) {
      return "SquadronCommander";
    }

    if (store.state.user?.is_staff) {
      return "KeyCommander";
    }

    if (store.state.user) {
      return "Executor";
    }

    return "NotAuth";
  }

  export default store;
import axios from "axios";
import { setAuthorizationToken } from "./authUtils";

const spec_url = "http://127.0.0.1:8000/users/specialization/";
const handlings_levels_url = "http://127.0.0.1:8000/handlings/handlings_level/";
const airship_types_url = "http://127.0.0.1:8000/handlings/airship_type/";
const airship_url = "http://127.0.0.1:8000/handlings/airship/";
const technologic_cards_url = "http://127.0.0.1:8000/handlings/technologic_card/";
const jzot_url = "http://127.0.0.1:8000/handlings/jzot/";
const card_handlings_url = "http://127.0.0.1:8000/handlings/card_handlings/";
const card_header_url = "http://127.0.0.1:8000/handlings/card_header/";
const extended_users_partial = "http://127.0.0.1:8000/users/extended_users_partial/";
const complete_execute_url = "http://127.0.0.1:8000/handlings/complete_execute/";

async function getFromUrl(url, params) {
  try {
    const response = await axios.get(url, { params });
    console.log("response after get from ", url, ":");
    console.log(response);
    return response.data;
  } catch (error) {
      console.log("cannot get resource from url: ", url, " error was occured: ", error);
  }
}

export async function deleteDataBaseObject(url) {
  try {
    const response = await axios.delete(url);
    console.log("response after delete from  ", url, ":");
    console.log(response);
    return response;
  } catch (error) {
    console.log("cannot delete resource from url: ", url, " error was occured: ", error);
  }
}


export async function createDataBaseObject(url, db_object, table_name) {
  try {
    setAuthorizationToken(localStorage.getItem("jwtAccessToken"));
    const response = await axios.post(url, db_object);
    console.log("response after post from ", url, ":");
    console.log(response);
    if (response.statusText === "Created") {
        window.alert(table_name + " created");
    }
  } catch (error) {
      console.log(table_name + " creation error", error);
      if (error.response) {
        console.log(table_name + " creation error: ", error.response);
        let message = "";
        if (error.response.data) {
            for (let [key, value] of Object.entries(error.response.data)) {
            message += `${key}: ${value.join()} \n`;
            }
        }
        throw message
      }
  }
}

export async function putDataBaseObject(url, db_object, table_name) {
  try {
    setAuthorizationToken(localStorage.getItem("jwtAccessToken"));
    const response = await axios.put(url, db_object);
    console.log("response after put from ", url, ":");
    console.log(response);
    if (response.statusText === "OK") {
        window.alert(table_name + " edited");
    }
  } catch (error) {
      console.log(table_name + " edit error", error);
      if (error.response) {
        console.log(table_name + " edit error: ", error.response);
        let message = "";
        if (error.response.data) {
            for (let [key, value] of Object.entries(error.response.data)) {
            message += `${key}: ${value.join()} \n`;
            }
        }
        throw message
      }
  }
}

export async function getSpecializations() {
  return await getFromUrl(spec_url);
}

export async function getHandlingsLevels() {
  return await getFromUrl(handlings_levels_url);
}

export async function getAirshipTypes() {
  return await getFromUrl(airship_types_url);
}

export async function getAirshipTypeById(id) {
  return await getFromUrl(airship_types_url + id + "/");
}

export async function getAirships() {
  return await getFromUrl(airship_url);
}

export async function getAirshipById(id) {
  return await getFromUrl(airship_url + id + "/");
}

export async function getTechnologicCards() {
  return await getFromUrl(technologic_cards_url);
}

export async function getTechnologicCardById(id) {
  return await getFromUrl(technologic_cards_url + id + "/");
}

export async function getJzot(params) {
  return await getFromUrl(jzot_url, params);
}

export async function getJzotById(id) {
  return await getFromUrl(jzot_url + id + "/");
}

export async function getCardHandlings(params) {
  return await getFromUrl(card_handlings_url, params);
}

export async function getCardHandlingById(id) {
  return await getFromUrl(card_handlings_url + id + "/");
}

export async function getCardHeader(params) {
  return await getFromUrl(card_header_url, params);
}

export async function getCardHeaderById(id) {
  return await getFromUrl(card_header_url + id + "/");
}

export async function getExtendedUsersPartial() {
  return await getFromUrl(extended_users_partial);
}

export async function getSupervisorsPartial(spec) {
  // @TODO Handle this in backend
  const params = {
    filters: {
      is_staff: true,
      is_active: true
    }
  }

  if (spec) {
    params.filters.specialization = spec
  }
  return await getFromUrl(extended_users_partial, params);
}

export async function getExecutorsPartial(spec) {
  // @TODO Handle this in backend (Should add this ?)
  const params = {
    filters: {
      is_staff: false,
      is_admin: false,
      is_active: true
    }
  }

  if (spec) {
    params.filters.specialization = spec
  }

  return await getFromUrl(extended_users_partial, params);
}

// @TODO Refactor this
export async function completeExecute(id, patch) {
  try {
    setAuthorizationToken(localStorage.getItem("jwtAccessToken"));
    const response = await axios.patch(complete_execute_url + id, patch);
    console.log("response after patch from ", complete_execute_url + id, ":");
    console.log(response);
    if (response.statusText === "OK") {
        window.alert("Execute" + " edited");
    }
  } catch (error) {
      console.log("Execute" + " edit error", error);
      if (error.response) {
        console.log("Execute" + " edit error: ", error.response);
        let message = "";
        if (error.response.data) {
            for (let [key, value] of Object.entries(error.response.data)) {
            message += `${key}: ${value.join()} \n`;
            }
        }
        throw message
      }
  }
}

// @TODO Refactor this
export async function approveHeader(id, patch) {
  try {

    let approve_url = card_header_url + id + "/approve/"

    setAuthorizationToken(localStorage.getItem("jwtAccessToken"));
    const response = await axios.patch(approve_url, patch);
    console.log("response after patch from ", approve_url, ":");
    console.log(response);
    if (response.statusText === "OK") {
        //window.alert("Card Approved");
        console.log("Card Approved")
    }
  } catch (error) {
      console.log("Card Header approve" + "error", error);
      if (error.response) {
        console.log("Card Header approve" + "error: ", error.response);
        let message = "";
        if (error.response.data) {
            for (let [key, value] of Object.entries(error.response.data)) {
            message += `${key}: ${value.join()} \n`;
            }
        }
        throw message
      }
  }
}

// @TODO Refactor this
export async function finishHeader(id, patch) {
  try {

    let finish_url = card_header_url + id + "/finish/"

    setAuthorizationToken(localStorage.getItem("jwtAccessToken"));
    const response = await axios.patch(finish_url, patch);
    console.log("response after patch from ", finish_url, ":");
    console.log(response);
    if (response.statusText === "OK") {
        console.log("Card finished")
    }
  } catch (error) {
      console.log("Card Header finish" + "error", error);
      if (error.response) {
        console.log("Card Header finish" + "error: ", error.response);
        let message = "";
        if (error.response.data) {
            for (let [key, value] of Object.entries(error.response.data)) {
            message += `${key}: ${value.join()} \n`;
            }
        }
        throw message
      }
  }
}
import axios from "axios";
import jwtDecode from "jwt-decode";

export function setAuthorizationToken(
  token = localStorage.getItem("jwtAccessToken")
) {
  if (token) {
    axios.defaults.headers.common["Authorization"] = `JWT ${token}`;
  } else {
    delete axios.defaults.headers.common["Authorization"];
  }
}

export function loginHandle(response) {
  const accessToken = response.data.access;
  const refreshToken = response.data.refresh;
  localStorage.setItem("jwtAccessToken", accessToken);
  localStorage.setItem("jwtrefreshToken", refreshToken);
}

export function logoutHandle() {
  localStorage.removeItem("jwtAccessToken");
  localStorage.removeItem("jwtrefreshToken");
}

export function IsTokenValid(token) {
  if (token) {
    var decodedToken = jwtDecode(token, { complete: true });
    var dateNow = new Date();
    console.log("decodedToken: ", decodedToken);
    console.log("dateNow: ", dateNow.getTime());
    return decodedToken.exp >= dateNow.getTime() / 1000;
  }
  return false;
}

export function getUserIdFromToken(
  token = localStorage.getItem("jwtAccessToken")
) {
  if (token && IsTokenValid(token)) {
    return jwtDecode(token, { complete: true }).user_id;
  }
  return null;
}

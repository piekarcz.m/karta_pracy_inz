import { createRouter, createWebHistory } from 'vue-router';
import Home from "../views/Home.vue";
import Login from "../views/Login.vue";
import Register from "../views/Register.vue";
import UserInfo from "../views/UserInfo.vue";
import CardHandling from "../views/CardHandling.vue";
import CardHeader from "../views/CardHeader.vue";
import Jzot from "../views/Jzot.vue";
import AirshipType from "../views/AirshipType.vue";
import AirshipTypeRecord from "@/components/airship_type/AirshipTypeRecord.vue";
import TechnologicCard from "../views/TechnologicCard.vue";
import Logout from "../views/Logout.vue";
import { IsTokenValid } from "../utils/authUtils";

import store from "../store";

// @TODO add one more level of auth : isStaff - (jzot should have that level for create) -- probably done
const routes = [
  { path: "/", component: Home, meta: { requiresAuth: false, onlyForNotAuth: false } },
  { path: "/login", component: Login, meta: { requiresAuth: false, onlyForNotAuth: true } },
  { path: "/logout", component: Logout, meta: { requiresAuth: false, onlyForNotAuth: false } },
  { path: "/register", component: Register, meta: { requiresAuth: false, onlyForNotAuth: true } },
  { path: "/user_info", component: UserInfo, meta: { requiresAuth: true, onlyForNotAuth: false } },

  {
    path: "/card_handlings/",
    component: CardHandling, meta: { requiresAuth: true, onlyForNotAuth: false },
    props: route => ({
      header_id: route.query.header_id,
      handling_id: route.query.handling_id,
      airship_type_id: route.query.airship_type_id,
    })
  },

  { path: "/card_header", component: CardHeader, meta: { requiresAuth: true, onlyForNotAuth: false } },

  { path: "/jzot", component: Jzot, meta: { requiresAuth: true, onlyForNotAuth: false } },
  { path: "/jzot/:id", component: Jzot, meta: { requiresAuth: true, onlyForNotAuth: false }, props: true },

  {
    path: "/airship_type/:id",
    component: AirshipTypeRecord, meta: { requiresAuth: true, onlyForNotAuth: false },
    props: true
  },
  {
    path: "/airship_type/:id/:inital_state",
    component: AirshipTypeRecord, meta: { requiresAuth: true, onlyForNotAuth: false },
    props: true
  },
  { path: "/airship_type", component: AirshipType, meta: { requiresAuth: true, onlyForNotAuth: false } },

  { path: "/technologic_card", component: TechnologicCard, meta: { requiresAuth: true, onlyForNotAuth: false } },
];

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: routes
});

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (store.state.isAuthenticated) {
      next();
    } else {
      next("/");
    }
  }
  else if (to.matched.some((record) => record.meta.onlyForNotAuth)) {
    if (store.state.isAuthenticated) {
      next("/");
    } else {
      next();
    }
  }
  else {
    next();
  }
});

export default router
